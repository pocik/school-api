variable "ec2_region" {
  default = "eu-central-1"
}

variable "ec2_ami" {
  default = "ami-0d527b8c289b4af7f"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_keypair" {
  default = "django_server"
}

variable "ec2_security_group" {
  default = "django_server"
}

variable "ec2_instance_name" {
  default = "django_server"
}