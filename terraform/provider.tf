provider "aws" {
  profile = "default"
  region  = var.ec2_region
}