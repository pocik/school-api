# Change the Debug to False 
# Change ALLOWED_HOSTS


sudo apt update
sudo apt-get install -y python3.8 python3.8-dev python3.8-distutils python3.8-venv # python 3.8
mkdir $HOME/.venv
python3 -m venv $HOME/.venv/school-api
source $HOME/.venv/school-api/bin/activate

git clone https://gitlab.com/pocik/school-api.git  #<REPO_URL> 

pip install -r $HOME/school-api/requirements.txt

# DOCS https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
sudo apt install -y gcc # Install GCC 
# pip install wheel # ADD to requirements.txt
# pip install uwsgi  # ADD to requirements.txt

# Nginx
sudo apt install -y nginx
sudo /etc/init.d/nginx start

# get nginx uwsgi params file make sure you are in right dir 
wget https://raw.githubusercontent.com/nginx/nginx/master/conf/uwsgi_params --directory-prefix=$HOME/school-api

# create new conf
sudo mv $HOME/school-api/setups/school_project_nginx.conf /etc/nginx/sites-available/mysite_nginx.conf
# simlink conf
sudo ln -s /etc/nginx/sites-available/mysite_nginx.conf /etc/nginx/sites-enabled/

# change the /etc/nginx/nginx.conf server_names_hash_bucket_size 64; to 128
sudo vim /etc/nginx/nginx.conf
sudo systemctl restart nginx

# edit settings for statics
# STATIC_ROOT = os.path.join(BASE_DIR, "static/")
python manage.py collectstatic


# http port 
uwsgi --chdir /home/ubuntu/school-api --http :8001 --module school_project.wsgi:application --home /home/ubuntu/.venv/school-api  

# socket port 
uwsgi --chdir /home/ubuntu/school-api --socket :8001 --module school_project.wsgi:application --home /home/ubuntu/.venv/school-api

